function displaySum(num1, num2) {
	let getSum = num1 + num2;
	console.log('Displayed sum of ' + num1 + ' and ' + num2);
	console.log(getSum);
}
displaySum(5, 15);

function displayDifference(num1, num2) {
	let getDifference = num1 - num2;
	console.log('Displayed difference of ' + num1 + ' and ' + num2);
	console.log(getDifference);
}
displayDifference(20, 5);

function displayProduct(num1, num2) {
	let getProduct = num1 * num2;
	console.log('The product of ' + num1 + " and " + num2 +":");
	console.log(getProduct);
}
displayProduct(50, 10);

function displayQuotient(num1, num2) {
	let getQuotient = num1 / num2;
	console.log('The quotient of ' + num1 + " and " + num2 + ":");
	console.log(getQuotient);
}
displayQuotient(50, 10);

function circleArea(radius) {
	let area = ((radius ** 2) * 3.14159);
	let areaOfCircle = Math.round(area*100)/100;
	console.log('The result of getting the area of a circle with 15 radius:');
	console.log(areaOfCircle);	
}
circleArea(15); //15

function averageVar(num1, num2, num3, num4) {
	let average = (num1 + num2 + num3 + num4)/4;
	console.log('The average of ' + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	console.log(average);
}
averageVar(20, 40, 60, 80); //20, 40, 60, 80

function isPassingScore(myScore, totalScore) {
	let getPercentage = (myScore / totalScore)*100;
	let isPassed = (getPercentage > 75) ? true : false;
	console.log('Is 38/50 a passing score?');
	console.log(isPassed);
}
isPassingScore(38, 50);